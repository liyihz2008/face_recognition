# -*- coding: utf-8 -*-
from face_recognition import db
from face_recognition.models.face_index import FaceDataIndex
from face_recognition.service.dao.face_info_dao import FaceDataDao
from face_recognition.service.dao.user_info_dao import UserInfoDao


class FaceInfoService:
	
	def __init__(self):
		self.face_info_dao = FaceDataDao(db)
		self.user_info_dao = UserInfoDao(db)
	
	def save(self, user, face_data_index, face_data):
		"""
		为用户保存一个人脸信息
		:param user: 用户信息
		:param face_data_index: 人脸信息
		:param face_data: 人脸数据信息（base64编码， 人脸特征）
		:return:user, face_data_index, face_data
		"""
		if user is None or face_data_index is None or face_data is None:
			raise Exception("需要保存的对象不合法，无法保存人脸信息")
		user = self.user_info_dao.get_with_id(user.id)
		if user is None:
			raise Exception("用户信息不存在，可能已经被删除了。ID=" + user.id)
		else:
			# 保存或者更新face_data_index
			face_data_index.id = FaceDataIndex.create_id()
			face_data_index.user_id = user.id
			# 保存或者更新face_data
			face_data.id = face_data_index.id
			face_data.user_id = user.id
			
			# 增加一个人脸图片计数
			if user.face_count is None:
				user.face_count = 0
			user.face_count = user.face_count + 1
			
			db.session.add(face_data_index)
			db.session.add(face_data)
			db.session.commit()
			return user, face_data_index, face_data
		return None
	
	def update_feature_with_id(self, id, feature_string):
		"""
		根据ID更新图片的人脸特征信息
		:param id:
		:param feature_string:
		:return:
		"""
		face_data = self.get_face_data_with_id(id)
		if face_data is not None:
			face_data.face_feature = feature_string
		db.session.commit()
	
	def get_face_data_with_id(self, id):
		"""
		根据D获取人脸数据信息对象
		:param id: 用户信息ID
		:return: FaceData
		"""
		return self.face_info_dao.get_face_data_with_id(id)
	
	def list_face_index_with_user_id(self, id):
		"""
		根据用户ID获取用户所有的人脸信息列表
		:param id: 用户信息ID
		:return: List<FaceDataIndex>
		"""
		return self.face_info_dao.list_face_index_with_user_id(id)
	
	def get_base64_with_face_id(self, id):
		"""
		根据Face_ID获取用户人脸图像的base64编码信息
		:param id: 人脸信息ID
		:return: base64(String)
		"""
		return self.face_info_dao.get_base64_with_face_id(id)
	
	def get_feature_with_face_id(self, id):
		"""
		根据Face_ID获取用户人脸特征信息
		:param id: 人脸信息ID
		:return: base64(String)
		"""
		return self.face_info_dao.get_feature_with_face_id(id)
	
	def delete_all_with_uid(self, uid):
		"""
		根据用户ID删除所有的人脸信息以及人脸数据
		:param uid: 用户UID
		:return:True | False
		"""
		return self.face_info_dao.delete_all_with_uid(uid)

