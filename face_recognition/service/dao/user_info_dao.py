# -*- coding: utf-8 -*-
from sqlalchemy import or_, func

from face_recognition.models.user_info import UserInfo
from face_recognition.utils.string_utils import StringUtils
from face_recognition.utils.log_utils import LoggerUtil


class UserInfoDao:
	def __init__(self, _db):
		self.db = _db
	
	def get_with_id(self, id):
		"""
		根据ID获取用户基础信息
		:param id:
		:return: user_info or None
		"""
		user = None
		if StringUtils.isNotEmpty(id):
			try:
				user = self.db.session.query(UserInfo).filter(UserInfo.id == id).first()
			except Exception as err:
				LoggerUtil.error("系统根据ID查询用户信息时发生异常！ID:" + id)
				raise err
		else:
			LoggerUtil.error("用户ID为空，无法查询信息！")
		return user
	
	def get_with_employee_no(self, employee_no):
		"""
		根据employee_no获取用户基础信息
		:param employee_no:
		:return: user_info or None
		"""
		user = None
		if StringUtils.isNotEmpty(employee_no):
			try:
				user = self.db.session.query(UserInfo).filter(UserInfo.employee_no == employee_no).first()
			except Exception as err:
				LoggerUtil.error("系统根据EMPLOYEE_NO查询用户信息时发生异常！EMPLOYEE_NO:" + employee_no)
				raise err
		else:
			LoggerUtil.error("用户EMPLOYEE_NO为空，无法查询信息！")
		return user
	
	def get_with_index_number(self, index_number):
		"""
		根据employee_no获取用户基础信息
		:param employee_no:
		:return: user_info or None
		"""
		user = None
		if index_number is not None and index_number > 0:
			try:
				user = self.db.session.query(UserInfo).filter(UserInfo.index_number == index_number).first()
			except Exception as err:
				LoggerUtil.error("系统根据INDEX_NUMBER查询用户信息时发生异常！INDEX_NUMBER:" + index_number)
				raise err
		else:
			LoggerUtil.error("用户INDEX_NUMBER为空，无法查询信息！")
		return user
	
	def list_with_key(self, key):
		"""
		根据关键字获取用户基础信息：适用姓名，ID，员工号
		:param key: 关键字
		:return: user_info or None
		"""
		users = None
		if StringUtils.isNotEmpty(key):
			try:
				users = self.db.session.query(UserInfo).filter(
					or_(UserInfo.username.like(key), UserInfo.id.like(key), UserInfo.employee_no.like(key), UserInfo.email.like(key))
				).all()
			except Exception as err:
				LoggerUtil.error("系统根据关键字查询用户信息时发生异常！KEY:" + key)
				raise err
		else:
			LoggerUtil.error("关键字KEY为空，无法查询信息！")
		return users
	
	def list_all_user_ids(self):
		"""
		获取所有的用户ID列表
		:return: List<UserInfo.id>
		"""
		try:
			return self.db.session.query(UserInfo.id).all()
		except Exception as err:
			LoggerUtil.error("系统查询全部用户ID列表时发生异常")
			raise err
		
	def list_collect_over_user_ids(self):
		"""
		获取所有人脸信息收集完成，并且未训练过的用户ID列表
		:return: List<String>
		"""
		try:
			return self.db.session.query(UserInfo.id).filter(UserInfo.face_status == 1).all()
		except Exception as err:
			LoggerUtil.error("系统获取所有人脸信息收集完成，并且未训练过的用户ID列表时发生异常")
			raise err
		
	def list_all_users(self):
		"""
		获取所有的用户列表
		:return: List<UserInfo>
		"""
		try:
			return self.db.session.query(UserInfo).all()
		except Exception as err:
			LoggerUtil.error("系统查询全部用户列表时发生异常")
			raise err

	def get_max_index_number(self):
		"""
		获取用户最大的顺序编号
		:return: index_number
		"""
		try:
			result = self.db.session.query(func.max(UserInfo.index_number)).first()
			if result is None or result[0] is None:
				return 0
			else:
				return result[0]
		except Exception as err:
			LoggerUtil.error("系统获取用户最大的顺序编号时发生异常")
			raise err