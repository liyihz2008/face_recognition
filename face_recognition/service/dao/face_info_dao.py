# -*- coding: utf-8 -*-
from face_recognition.models.face_data import FaceData
from face_recognition.models.face_index import FaceDataIndex
from face_recognition.models.user_info import UserInfo
from face_recognition.utils.array_utils import ListTools
from face_recognition.utils.string_utils import StringUtils
from face_recognition.utils.log_utils import LoggerUtil


class FaceDataDao:
	
	def __init__(self, _db):
		self.db = _db
	
	def get_face_data_with_id(self, id):
		"""
		根据人脸图像ID获取人脸数据信息
		:param id:
		:return:FaceData
		"""
		if StringUtils.isNotEmpty(id):
			try:
				return self.db.session.query(FaceData).filter(FaceData.id == id).first()
			except Exception as err:
				LoggerUtil.error("系统根据人脸图像ID查询人脸信息时发生异常！ID:" + id)
				raise err
		else:
			LoggerUtil.error("人脸图像ID为空，无法查询信息！")
		return None
	def list_face_index_with_user_id(self, id):
		"""
		根据用户ID获取用户所有的人脸信息列表
		:param id: 用户信息ID
		:return: List<FaceDataIndex>
		"""
		face_data_index_list = None
		if StringUtils.isNotEmpty(id):
			try:
				face_data_index_list = self.db.session.query(FaceDataIndex).filter(FaceDataIndex.user_id == id).all()
			except Exception as err:
				LoggerUtil.error("系统根据用户ID查询人脸信息列表时发生异常！ID:" + id)
				raise err
		else:
			LoggerUtil.error("用户UID为空，无法查询信息！")
		return face_data_index_list
	
	def get_base64_with_face_id(self, id):
		"""
		根据Face_ID获取用户人脸图像的base64编码信息
		:param id: 人脸信息ID
		:return: base64(String)
		"""
		if StringUtils.isNotEmpty(id):
			try:
				face_data = self.db.session.query(FaceData).filter(FaceData.id == id).first()
				if face_data is not None:
					return face_data.base64_content
				else:
					LoggerUtil.error("貌似需要查询的人脸信息并不存在！ID:" + id)
			except Exception as err:
				LoggerUtil.error("系统根据人脸信息ID查询人脸信息时发生异常！ID:" + id)
				raise err
		else:
			LoggerUtil.error("人脸信息ID为空，无法查询信息！")
		return None
	
	def get_feature_with_face_id(self, id):
		"""
		根据Face_ID获取用户人脸特征信息
		:param id: 人脸信息ID
		:return: base64(String)
		"""
		if StringUtils.isNotEmpty(id):
			try:
				face_data = self.db.session.query(FaceData).filter(FaceData.id == id).first()
				if face_data is not None:
					return face_data.face_feature
				else:
					LoggerUtil.error("貌似需要查询的人脸信息并不存在！ID:" + id)
			except Exception as err:
				LoggerUtil.error("系统根据人脸信息ID查询人脸特征信息时发生异常！ID:" + id)
				raise err
		else:
			LoggerUtil.error("人脸信息ID为空，无法查询信息！")
		return None
	
	def delete_all_with_uid(self, uid):
		"""
		根据用户ID删除所有的人脸信息以及人脸数据
		:param uid: 用户UID
		:return:True | False
		"""
		try:
			face_data_indexes = self.db.session.query(FaceDataIndex).filter(FaceDataIndex.user_id == uid).all()
			face_datas = self.db.session.query(FaceData).filter(FaceData.user_id == uid).all()
			user = self.db.session.query(UserInfo).filter(UserInfo.id == uid).first()
			if ListTools.isNotEmpty(face_data_indexes):
				for face_data_indexe in face_data_indexes:
					self.db.session.delete(face_data_indexe)
			if ListTools.isNotEmpty(face_datas):
				for face_data in face_datas:
					self.db.session.delete(face_data)
			user.face_count = 0
			self.db.session.commit()
			return True
		except Exception as err:
			raise err
