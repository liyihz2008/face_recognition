# -*- coding: utf-8 -*-
import time

from face_recognition import LoggerUtil, feature_manager, model_manager
from face_recognition.service.user_info_service import UserInfoService
from face_recognition.utils.array_utils import ListTools


def check_for_update():
	"""
	检测数据库中是否存在人脸图片已经采集完成的用户信息，如果存在，则将该用户的人脸信息加入到已经训练好的认证模型里
	"""
	user_info_service = UserInfoService()
	user_ids = user_info_service.list_collect_over_user_ids()
	if ListTools.isNotEmpty(user_ids):
		for user_id in user_ids:
			user = user_info_service.get_with_id(user_id)
			if user is not None:
				features = feature_manager.load_face_features_with_user_id(user_id)
				if ListTools.isNotEmpty(features):
					feature_manager.update_features_and_labels(features, user.index_number)
		model_manager.train(feature_manager.train_faces, feature_manager.train_face_labels)
		# 更新所有用户的人脸特征训练状态为：训练完成-9
		for user_id in user_ids:
			user_info_service.train_over(user_id)
	LoggerUtil.info("定时任务：人脸收集状态更新检测任务执行完成！")


