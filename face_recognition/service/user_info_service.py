# -*- coding: utf-8 -*-
from face_recognition import db
from face_recognition.models.user_info import UserInfo
from face_recognition.service.dao.user_info_dao import UserInfoDao
from face_recognition.utils.array_utils import ListTools
from face_recognition.utils.string_utils import StringUtils


class UserInfoService:
	
	def __init__(self):
		self.user_info_dao = UserInfoDao(db)
	
	def save(self, user):
		"""
		保存一个用户信息
		:param user:
		:return:
		"""
		entity = None
		if user is not None and StringUtils.isNotEmpty(user.id):
			entity = UserInfo.query.filter_by(id=user.id).first()
		if entity is not None:
			# 更新
			entity.email = user.email
			entity.username = user.username
			entity.employee_no = user.employee_no
			entity.unit_id = user.unit_id
			entity.mobile = user.mobile
		else:
			# 新增
			index_number = self.user_info_dao.get_max_index_number()
			entity = UserInfo()
			entity.set_id(str(UserInfo.create_id()))
			entity.email = user.email
			entity.username = user.username
			entity.employee_no = user.employee_no
			entity.unit_id = user.unit_id
			entity.password_hash = user.password_hash
			entity.face_status = 0
			entity.face_count = 0
			entity.index_number = (index_number + 1)
			db.session.add(entity)
		db.session.commit()
		return entity
	
	def get_with_id(self, id):
		"""
		根据ID获取用户基础信息
		:param id:
		:return: user_info or None
		"""
		return self.user_info_dao.get_with_id(id)
	
	def get_with_employee_no(self, employee_no):
		"""
		根据employee_no获取用户基础信息
		:param employee_no:
		:return: user_info or None
		"""
		return self.user_info_dao.get_with_employee_no(employee_no)
	
	def get_with_index_number(self, index_number):
		"""
		根据employee_no获取用户基础信息
		:param employee_no:
		:return: user_info or None
		"""
		return self.user_info_dao.get_with_index_number(index_number)
	
	def list_with_key(self, key):
		"""
		根据关键字获取用户基础信息
		:param key:关键字
		:return: user_info or None
		"""
		return self.user_info_dao.list_with_key(key)
	
	def list_all_user_ids(self):
		"""
		获取所有的用户ID列表
		:return: List<String>
		"""
		results = self.user_info_dao.list_all_user_ids()
		if ListTools.isNotEmpty(results):
			return [x[0] for x in results]
		return []
	
	def list_collect_over_user_ids(self):
		"""
		获取所有人脸信息收集完成，并且未训练过的用户ID列表
		:return: List<String>
		"""
		results = self.user_info_dao.list_collect_over_user_ids()
		if ListTools.isNotEmpty(results):
			return [x[0] for x in results]
		return []
	
	def list_all_users(self):
		"""
		获取所有的用户ID列表
		:return: List<UserInfo>
		"""
		return self.user_info_dao.list_all_users()
	
	def delete(self, id):
		"""
		根据ID删除用户基础信息
		:param id:
		:return: True | False
		"""
		user = self.get_with_id(id)
		if user is not None:
			db.session.delete(user)
			db.session.commit()
			return True
		return False
	
	def wait_to_collect(self, id):
		"""
		将用户的人脸图像状态标识为待采集完成 - 0
		:param id:
		:return: user_info or None
		"""
		user = self.user_info_dao.get_with_id(id)
		if user is not None:
			user.face_status = 0
			db.session.commit()
		return user
	
	def collect_over(self, id):
		"""
		将用户的人脸图像状态标识为收集完成，待模型训练 - 1
		:param id:
		:return: user_info or None
		"""
		user = self.user_info_dao.get_with_id(id)
		if user is not None:
			user.face_status = 1
			db.session.commit()
		return user
	
	def train_over(self, id):
		"""
		将用户的人脸图像状态标识为特征训练完成 - 9
		:param id:
		:return: user_info or None
		"""
		user = self.user_info_dao.get_with_id(id)
		if user is not None:
			user.face_status = 9
			db.session.commit()
		return user
