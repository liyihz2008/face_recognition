# -*- coding: utf-8 -*-

from configparser import ConfigParser

face_config = ConfigParser()
face_config.read("config.properties")


def get_property_with_name(config_model, config_name):
	"""
	配置文件读取帮助类
	:param config_model:
	:param config_name:
	:return:
	"""
	return face_config.get(config_model, config_name)
