# -*- coding: utf-8 -*-

from flask_socketio import emit
from face_recognition import socketio
from face_recognition.views.wrapers import wraper_json_data, wraper_need_login


@socketio.on('my event', namespace='/test')
@wraper_json_data
@wraper_need_login
def test_message(message):
	emit('my response', {'data': message['data']})


@socketio.on('my broadcast event', namespace='/test')
@wraper_json_data
@wraper_need_login
def test_message(message):
	emit('my response', {'data': message['data']}, broadcast=True)


@socketio.on('connect', namespace='/test')
@wraper_json_data
@wraper_need_login
def test_connect():
	emit('my response', {'data': 'Connected'})


@socketio.on('disconnect', namespace='/test')
@wraper_json_data
@wraper_need_login
def test_disconnect():
	print('Client disconnected')
