# -*- coding: utf-8 -*-

from flask import request, jsonify
from face_recognition import app
from face_recognition.models.user_info import UserInfo, users_schema, user_schema
from face_recognition.service.face_info_service import FaceInfoService
from face_recognition.service.user_info_service import UserInfoService
from face_recognition.utils.string_utils import StringUtils
from face_recognition.views.wrapers import wraper_need_login, wraper_json_data


@app.route('/face_recognition/user', methods=['POST'])
@wraper_json_data
@wraper_need_login
def save():
	"""
	根据请求的数据，保存一个用户信息
	:return:
	"""
	userInfoService = UserInfoService()
	data = request.get_json()
	
	# 查询数据是否已经存在：ID一样即为存在
	if "id" in data.keys() and StringUtils.isNotEmpty(data["id"]):
		user = userInfoService.get_with_id(data["id"])
	
	if user is None:
		# 新用户的员工号不允许重复，查询数据是否已经存在：employee_no一样即为存在
		if "employee_no" in data.keys() and StringUtils.isNotEmpty(data["employee_no"]):
			user = userInfoService.get_with_employee_no(data["employee_no"])
			if user is not None:
				result = {"status": "error", "message": "相同工号的用户已经存在！工号:" + data["employee_no"]}
				return jsonify(result)
	
	# 组装用户信息过程
	user = UserInfo()
	if "id" in data.keys():
		user.set_id(data["id"])
	else:
		user.set_id(str(UserInfo.create_id()))
	if "user_email" in data.keys():
		user.email = data["user_email"]
	if "user_name" in data.keys():
		user.username = data["user_name"]
	if "employee_no" in data.keys():
		user.employee_no = data["employee_no"]
	if "unit_id" in data.keys():
		user.unit_id = data["unit_id"]
	if "mobile" in data.keys():
		user.mobile = data["mobile"]
	user.password_hash = "000000"
	
	# 将用户信息持久化到数据库
	userInfoService.save(user)
	result = {"status": "success", "message": "用户添加成功！"}
	return jsonify(result)


@app.route('/face_recognition/user/<uid>', methods=['DELETE'])
@wraper_json_data
@wraper_need_login
def delete(uid):
	"""
	根据传入的UID，删除一个用户信息，并且清除所有与该用户有关的人脸数据信息
	:param uid:
	:return:
	"""
	userInfoService = UserInfoService()
	faceDataService = FaceInfoService()
	# 查询数据是否已经存在：ID一样即为存在
	if StringUtils.isNotEmpty(uid):
		faceDataService.delete_all_with_uid(uid)
		userInfoService.delete(uid)
		result = {"status": "success", "message": "用户删除成功！ID:" + uid}
		return jsonify(result)


@app.route('/face_recognition/user/list', methods=['PUT'])
@wraper_json_data
@wraper_need_login
def list_with_filter():
	"""
	根据条件查询用户列表
	:return:
	"""
	userInfoService = UserInfoService()
	data = request.get_json()
	# 查询数据是否已经存在：ID一样即为存在
	if "key" in data.keys() and StringUtils.isNotEmpty(data["key"]):
		key = data["key"]
		users = userInfoService.list_with_key(key)
	else:
		users = userInfoService.list_all_users()
	result = {"status": "success", "message": "用户查询完成！", "data": users_schema.dump(users).data}
	return jsonify(result)


@app.route('/face_recognition/user/<uid>', methods=['GET'])
@wraper_json_data
@wraper_need_login
def getUser(uid):
	"""
	根据传入的UID，查询一个用户信息
	:param uid:
	:return:
	"""
	if StringUtils.isNotEmpty(uid):
		userInfoService = UserInfoService()
		user = userInfoService.get_with_id(uid)
	if user is not None:
		result = {"status": "success", "message": "用户查询完成！", "data": user_schema.dump(user).data}
	else:
		result = {"status": "error", "message": "用户不存在，UID:" + uid}
	return jsonify(result)
