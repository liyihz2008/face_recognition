# -*- coding: utf-8 -*-

from flask import redirect
from face_recognition import app
from face_recognition.views.wrapers import wraper_need_login


@app.route('/face_recognition/main_user_list', methods=['GET'])
@wraper_need_login
def main_user_list():
	return redirect('face_recognition/main_user_list.html')


@app.route('/face_recognition/main_data_list', methods=['GET'])
@wraper_need_login
def main_data_list():
	return redirect('face_recognition/main_data_list.html')


@app.route('/face_recognition/main_log_list', methods=['GET'])
@wraper_need_login
def main_log_list():
	return redirect('face_recognition/main_log_list.html')


@app.route('/face_recognition/main_config_list', methods=['GET'])
@wraper_need_login
def main_config_list():
	return redirect('face_recognition/main_config_list.html')
