# -*- coding: utf-8 -*-

from face_recognition import db
from face_recognition.models.SerializeMixin import SerializeMixin


class BaseModelMixin(SerializeMixin):
	""" 定义基础的Model用于继承 """
	__table_args__ = {
		'mysql_engine': u'InnoDB',
		'mysql_default_charset': u'utf8'
	}
	
	def __repr__(self):
		if hasattr(self, 'name'):
			return '<%s %s>' % (self.__class__.__name__, self.name)
		else:
			return '<%s %s>' % (self.__class__.__name__, self.id)
	
	def _get_public(self):
		""" 当存在外键的时候, 一个友好的显示名称 """
		ret = self
		try:
			if hasattr(self, 'name'):
				rt = self.name
			elif hasattr(self, 'id'):
				rt = self.id
		except Exception as ex:
			print('_get_public error:%s' % str(ex))
		return rt
	
	def save(self):
		db.session.add(self)
		db.session.commit()
	
	def delete(self):
		db.session.delete(self)
		db.session.commit()
