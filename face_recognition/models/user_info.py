# -*- coding: utf-8 -*-

import uuid
from face_recognition import db, ma
from face_recognition.models.BaseModelMixin import BaseModelMixin
from face_recognition.utils.string_utils import StringUtils


class UserInfo(db.Model, BaseModelMixin):
	id = db.Column(db.String(128), primary_key=True)
	# 每次重新【全量】训练模型会为所有用户进行编序号
	# 这样可以免去在训练模型时将Lable转换为数字的过程，直接使用index_number就行
	index_number = db.Column(db.Integer(), default=1)
	username = db.Column(db.String(128), index=True, nullable=False)
	employee_no = db.Column(db.String(128), index=True, unique=True)
	email = db.Column(db.String(120))
	mobile = db.Column(db.String(11))
	password_hash = db.Column(db.String(128))
	unit_id = db.Column(db.String(128))
	face_count = db.Column(db.Integer, default=0)
	# 人脸训练状态，0 - 待收集完成， 1 - 已收集完成，待训练，9 - 训练完成
	face_status = db.Column(db.Integer, default=0)
	
	def __repr__(self):
		return '<Test %r>' % self.name
	
	def set_id(self, _id):
		if _id is None or StringUtils.isEmpty(_id):
			self.id = UserInfo.create_id()
		else:
			self.id = _id
	
	@staticmethod
	def create_id():
		"""
		生成一个唯一的64位UUID编码\n
		:return: like '525f1e08-debf-4fdc-9f73-47d67f2503af'
		"""
		return str(uuid.uuid4())


class UserInfoSchema(ma.Schema):
	class Meta:
		# Fields to expose
		fields = ('id', 'username', 'index_number', 'employee_no', 'email', 'mobile', 'unit_id', 'face_count')


user_schema = UserInfoSchema()
users_schema = UserInfoSchema(many=True)
