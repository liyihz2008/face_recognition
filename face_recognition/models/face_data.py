# -*- coding: utf-8 -*-

from face_recognition import db
from face_recognition.models.BaseModelMixin import BaseModelMixin


class FaceData(db.Model, BaseModelMixin):
	id = db.Column(db.String(128), primary_key=True)
	user_id = db.Column(db.String(128), nullable=False, index=True)
	base64_content = db.Column(db.Text, nullable=False)
	face_feature = db.Column(db.Text)

