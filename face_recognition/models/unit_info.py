# -*- coding: utf-8 -*-

from face_recognition import db
import uuid

from face_recognition.models.BaseModelMixin import BaseModelMixin


class UnitInfo(db.Model, BaseModelMixin):
	id = db.Column(db.String(128), primary_key=True)
	parent_id = db.Column(db.String(128), index=True)
	unitName = db.Column(db.String(64), index=True, unique=True)
	email = db.Column(db.String(120))
	admin_id = db.Column(db.String(128))
	description = db.Column(db.String(128))
	
	@staticmethod
	def create_id():
		"""
		生成一个唯一的64位UUID编码\n
		:return: like '525f1e08-debf-4fdc-9f73-47d67f2503af'
		"""
		return str(uuid.uuid4())
