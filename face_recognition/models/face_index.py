# -*- coding: utf-8 -*-

import uuid
from face_recognition import db, ma
from face_recognition.models.BaseModelMixin import BaseModelMixin
from face_recognition.utils.string_utils import StringUtils


class FaceDataIndex(db.Model, BaseModelMixin):
	id = db.Column(db.String(128), primary_key=True)
	user_id = db.Column(db.String(128), index=True)
	file_name = db.Column(db.String(128), index=True)
	file_status = db.Column(db.String(16), index=True)
	create_time = db.Column(db.DateTime)
	
	@staticmethod
	def create_id():
		"""
		生成一个唯一的64位UUID编码\n
		:return: like '525f1e08-debf-4fdc-9f73-47d67f2503af'
		"""
		return str(uuid.uuid4())
	
	def set_id(self, _id):
		if _id is None or StringUtils.isEmpty(_id):
			self.id = FaceDataIndex.create_id()
		else:
			self.id = _id
	
	class FaceDataIndexSchema(ma.Schema):
		class Meta:
			# Fields to expose
			fields = ('id', 'user_id', 'file_name', 'file_status', 'create_time')
	
	face_data_index_schema = FaceDataIndexSchema()
	face_data_index_schema = FaceDataIndexSchema(many=True)