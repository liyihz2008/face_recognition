# !flask/bin/python
# -*- coding: utf-8 -*-
from flask_apscheduler import APScheduler
from face_recognition import app

# create scheduler
scheduler = APScheduler()
# 定时任务的具体内容配置在config.py里 JOBS
scheduler.init_app(app)
# trigger schduler
scheduler.start()

app.run(debug=False)