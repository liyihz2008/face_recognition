# -*- coding: utf-8 -*-
import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
	APPLICATION_ROOT = "face_recognition"
	SERVER_NAME = "127.0.0.1:8080"
	# 配置数据库mysql+pymysql://username:password@server/db
	SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:root@localhost/face_recognition'
	SQLALCHEMY_TRACK_MODIFICATIONS = True
	SECRET_KEY = 'you-will-never-guess'
	SQLALCHEMY_POOL_SIZE = 5
	SQLALCHEMY_MAX_OVERFLOW = 10
	
	UPLOAD_FOLDER = 'face_images/'
	ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
	MAX_FACE_COUNT = 100  # 每个用户采集的最大数量
	MAX_CONTENT_LENGTH = 1024 * 1024  # 1MB
	MIN_CONFIDENCE = 80
	
	SAVE_MODEL_FILE = "face_recognition/recognition/dump/dump_recognition_model.xml"
	PICKLE_FEATURES_FILE = "face_recognition/recognition/dump/dump_face_features.pcl"
	PICKLE_FEATURE_LABLES_FILE = "face_recognition/recognition/dump/dump_face_feature_labels.pcl"
	
	JOBS = [
		{
			'id': 'job_run_check_for_update',
			'func': 'face_recognition:run_check_for_update',
			'trigger': 'interval',
			'seconds': 60
		}
	]
